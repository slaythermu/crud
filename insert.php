<!DOCTYPE html>
<html lang="pt-br">

<head>
    <!-- Meta tags Obrigatórias -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

    <title>Sistema Crud</title>
</head>
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="index.php">Home</a></li>
        <li class="breadcrumb-item">Cadastro</a></li>
        <li class="breadcrumb-item"><a href="produtos.php">Produtos</a></li>
        <li class="breadcrumb-item"><a href="categorias.php">Categorias</a></li>
    </ol>
</nav>

<body>
    <div class="container">
        <div class="row">
            <div class="col">
                <?php
                include "conexao.php";

                $nome = $_POST['nome'];
                $sku = $_POST['sku'];
                $descricao = $_POST['descricao'];
                $quantidade = $_POST['quantidade'];
                $preco = $_POST['preco'];
                $categoria = $_POST['categoria'];

                $sql = "INSERT INTO `produto` (`nome`, `sku`, `preco`, `descricao`, `quantidade`, `categoria`) VALUES ($nome, $sku, $preco, $descricao, $quantidade, $categoria)";

                if (mysqli_query($conn, $sql)) {
                    echo "Produto '$nome' cadastrado com sucesso !";
                } else
                    echo "$nome NÃO Cadastrado !";
                ?>
                <p>
                <p>
                <p>

                <a class="btn btn-warning" href="cadastro.php" role="button">Voltar</a>
            </div>
        </div>
    </div>


    <!-- JavaScript (Opcional) -->
    <!-- jQuery primeiro, depois Popper.js, depois Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
</body>

</html>