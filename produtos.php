<!DOCTYPE html>
<html lang="pt-br">

<head>
  <!-- Meta tags Obrigatórias -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

  <title>Sistema Crud</title>
</head>
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="index.php">Home</a></li>
    <li class="breadcrumb-item"><a href="cadastro.php">Cadastro</a></li>
    <li class="breadcrumb-item">Produtos</a></li>
    <li class="breadcrumb-item"><a href="categorias.php">Categorias</a></li>
  </ol>
</nav>

<body>
  <div class="container">
    <div class="row">
      <div class="col-sm">

        <?php
        $pesquisa = $_POST['busca'] ?? '';
        include "conexao.php";
        $sql = "SELECT * FROM produto WHERE sku LIKE '%$pesquisa%'";
        $dados = mysqli_query($conn, $sql);

        ?>
        <!-- Main Content -->
        <form class="form-inline" action="produtos.php" method="POST">
          <input class="form-control mr-sm-2" type="search" placeholder="Pesquisar" aria-label="Pesquisar" name="busca" autofocus>
          <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Pesquisar</button>
        </form>
        <p>
          <p>
            <table class="table table-hover">
              <thead>
                <tr>
                  <th scope="col">Nome</th>
                  <th scope="col">SKU</th>
                  <th scope="col">Preço</th>
                  <th scope="col">Descrição</th>
                  <th scope="col">Quantidade</th>
                  <th scope="col">Categorias</th>
                  <th scope="col">Funções</th>
                </tr>
              </thead>
              <tbody>
                <?php
                while ($linha = mysqli_fetch_assoc($dados)) {
                  $id = $linha['id'];
                  $nome = $linha['nome'];
                  $sku = $linha['sku'];
                  $preco = $linha['preco'];
                  $descricao = $linha['descricao'];
                  $quantidade = $linha['quantidade'];
                  $categoria = $linha['categoria'];

                  echo "<tr>
                    <td scope='row'>$nome</td>
                    <td>$sku</td>
                    <td>$preco</td>
                    <td>$descricao</td>
                    <td>$quantidade</td>
                    <td>$categoria</td>
                    <td><a class='btn btn-success btn-sm' href='cadastro_update.php?id=$id' role='button'>Editar</a>
                    <a class='btn btn-danger btn-sm' href='#' role='button'>Exluir</a>
                    </td>
                    </tr>";
                } ?>
              </tbody>
            </table>
          </p>
        </p>
      </div>
    </div>
  </div>


  <!-- JavaScript (Opcional) -->
  <!-- jQuery primeiro, depois Popper.js, depois Bootstrap JS -->
  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
</body>

</html>