# Projeto CRUD em PHP - Bootstrap, PDO & MySQL

Desenvolvendo de um simples projeto CRUD catálogo de produtos com categorias utilizando o acesso a banco de dados com o MySQL e linguagem PHP.

## Assuntos Abordados no Desenvolvimento do Projeto:

- Acesso a banco de dados com o MySql
- Uso de linguagens, como: PHP e CSS
- Uso do Bootstrap para realização de um layout responsivo para o projeto.

## Configuração do Projeto:

- Executar a query crud.sql ou importar o arquivo no phpMyAdmin para criar a table necessária.
- Editar o arquivo **conexao.php** 

```
$server = "nomeDoDominioOuIP"; //coloque a URL do seu DB
$user = "usuario"; //preencha o login do seu DB
$pass = "senha"; //preencha a senha do seu DB
$bd = "crud"; //selecione o DB que irá usar os dados

```